﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using wp_tp7.Resources;
using System.Windows.Media;

namespace wp_tp7
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Brush colorOrange = new SolidColorBrush(Colors.Orange);
        private Brush colorOriginale;
        private TranslateTransform translation = new TranslateTransform();
        private ScaleTransform resize = new ScaleTransform();
        private TransformGroup group = new TransformGroup();

        public MainPage()
        {
            InitializeComponent();

            // On associe les transformations au groupe
            // On change l'ordre pour évité de faire bouger le rectangle quand on zoom
            group.Children.Add(resize);
            group.Children.Add(translation);

            // On associe le groupe au rectangle
            rectangle.RenderTransform = group;
        }

        private void rectangle_ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            colorOriginale = rectangle.Fill;
            rectangle.Fill = colorOrange;
        }

        private void rectangle_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
            rectangle.Fill = colorOriginale;
        }

        private void rectangle_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            // On transmet le vecteur de translation de l'evenement à l'objet de translation
            translation.X += e.DeltaManipulation.Translation.X;
            translation.Y += e.DeltaManipulation.Translation.Y;

            // On ajoute un peu de débug
            Xvalue.Text = e.CumulativeManipulation.Scale.X.ToString();
            Yvalue.Text = e.CumulativeManipulation.Scale.Y.ToString();

            // On s'assure de ne pas avoir un coefficient null
            if (e.PinchManipulation != null)
            {
                // On applique le redimensionnement
                //resize.ScaleX *= e.DeltaManipulation.Scale.X;
                //resize.ScaleY *= e.DeltaManipulation.Scale.Y;
                
                // La c'est mieu :]
                // On passe la distance
                resize.ScaleX = e.PinchManipulation.CumulativeScale;
                resize.ScaleY = e.PinchManipulation.CumulativeScale;
            }
        }
    }
}